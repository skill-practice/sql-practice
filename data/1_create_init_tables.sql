CREATE TABLE students
(
    id          BIGSERIAL PRIMARY KEY,
    first_name  VARCHAR(512) NOT NULL,
    second_name VARCHAR(512),
    last_name   VARCHAR(512) NOT NULL
);

CREATE TABLE subjects
(
    id    BIGSERIAL PRIMARY KEY,
    title VARCHAR(512) NOT NULL
);

CREATE TABLE student_skills
(
    id         BIGSERIAL PRIMARY KEY,
    student_id BIGINT      NOT NULL REFERENCES students (id),
    subject_id BIGINT      NOT NULL REFERENCES subjects (id),
    skill_type VARCHAR(32) NOT NULL
);

ALTER TABLE student_skills
    ADD CONSTRAINT uq_sk__student_id__skill_id
        UNIQUE (student_id, subject_id);

CREATE TABLE exams
(
    id         BIGSERIAL PRIMARY KEY,
    title      VARCHAR(512) NOT NULL,
    subject_id BIGINT       NOT NULL REFERENCES subjects (id)
);

CREATE TABLE student_exams_scores
(
    id         BIGSERIAL PRIMARY KEY,
    student_id BIGINT  NOT NULL REFERENCES students (id),
    exam_id    BIGINT  NOT NULL REFERENCES exams (id),
    score      INTEGER NOT NULL
);

ALTER TABLE student_exams_scores
    ADD CONSTRAINT uq_ses__student_id__exam_id
        UNIQUE (student_id, exam_id);

