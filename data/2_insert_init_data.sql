CREATE OR REPLACE FUNCTION random_range(rangeStart INTEGER, rangeEnd INTEGER)
    RETURNS INTEGER AS
$$
BEGIN
    RETURN floor(random() * (rangeEnd - rangeStart + 1) + rangeStart);
END;
$$ language 'plpgsql' STRICT;

CREATE OR REPLACE FUNCTION random_text_simple(length INTEGER)
    RETURNS TEXT AS
$$
DECLARE
    possible_chars TEXT := 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    output         TEXT := '';
    i              INTEGER;
    pos            INTEGER;
    range          INTEGER;
BEGIN
    SELECT CASE
               WHEN length > length(possible_chars) THEN length(possible_chars)
               ELSE length END
    INTO range;

    FOR i IN 1..range
        LOOP
            pos := random_range(1, length(possible_chars));
            output := output || substr(possible_chars, pos, 1);
        END LOOP;
    RETURN output;
END;
$$ language 'plpgsql' STRICT;

CREATE OR REPLACE FUNCTION random_text_multiple(wordsCount INTEGER, wordLength INTEGER)
    RETURNS TEXT AS
$$
DECLARE
    output        TEXT := '';
    i             INTEGER;
    wordSeparator VARCHAR;
BEGIN
    CASE
        WHEN wordsCount = 1 THEN
            output := random_text_simple(wordLength);
        ELSE
            FOR i IN 1..wordsCount
                LOOP
                    SELECT CASE
                               WHEN random_range(0, 100) % 2 = 0 THEN '-'
                               ELSE ' ' END
                    INTO wordSeparator;
                    CASE
                        WHEN output = '' THEN
                            output := random_text_simple(random_range(1, wordLength));
                        ELSE
                            output := output || wordSeparator || random_text_simple(random_range(1, wordLength));
                        END CASE;
                END LOOP;
        END CASE;
    RETURN output;
END;
$$ language 'plpgsql' STRICT;

-- INSERT STUDENTS
DO
$$
    DECLARE
        i             INTEGER;
        hasSecondName BOOLEAN;
    BEGIN
        FOR i IN 1..100000
            LOOP
                SELECT CASE
                           WHEN random_range(1, 100) % 25 = 0 THEN FALSE
                           ELSE TRUE
                           END
                INTO hasSecondName;

                CASE
                    WHEN hasSecondName THEN
                        INSERT INTO students (first_name, second_name, last_name)
                        VALUES (random_text_simple(15), random_text_simple(15), random_text_simple(15));
                    ELSE
                        INSERT INTO students (first_name, second_name, last_name)
                        VALUES (random_text_simple(15), NULL, random_text_simple(15));
                    END CASE;
            END LOOP;
    END;
$$;

-- INSERT SUBJECTS
DO
$$
    DECLARE
        i INTEGER;
    BEGIN
        FOR i IN 1..50000
            LOOP
                INSERT INTO subjects (title)
                VALUES (random_text_multiple(random_range(1, 4), 30));
            END LOOP;
    END;
$$;

-- INSERT EXAMS
DO
$$
    DECLARE
        subject          subjects%ROWTYPE;
        examCount        INTEGER;
        randomExamsCount INTEGER;
    BEGIN
        FOR subject IN (SELECT * FROM subjects)
            LOOP
                SELECT random_range(1, 4) INTO randomExamsCount;
                FOR examCount IN 1..randomExamsCount
                    LOOP
                        INSERT INTO exams(title, subject_id)
                        VALUES ('EXAM ' || examCount || ': ' || subject.title, subject.id);
                    END LOOP;
            END LOOP;
    END;
$$;

CREATE INDEX idx_exams__subject_id ON exams(subject_id);

INSERT INTO student_exams_scores(student_id, exam_id, score)
SELECT ss.student_id,
       exam.id,
       random_range(1, 10)
FROM student_skills ss,
     exams exam
WHERE exam.subject_id = ss.subject_id;