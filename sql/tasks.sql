/*
    TASK 1:
    Select all primary skills that contain more than one word
        (please note that both ‘-‘ and ‘ ’ could be used as a separator).
*/
SELECT DISTINCT subject.*
FROM student_skills skill
         JOIN subjects subject on skill.subject_id = subject.id
WHERE skill.skill_type = 'PRIMARY'
  AND array_length(regexp_split_to_array(subject.title, '[\s\-]'), 1) > 1;
----------

/*
    TASK 2:
    Select all students who do not have a second name
        (it is absent or consists of only one letter/letter with a dot)
 */
SELECT *
FROM students
WHERE second_name IS NULL
   OR length(trim(second_name)) = 0
   OR second_name ~ '[\w]\.'
   OR length(second_name) = 1;
----------


/*
    TASK 3:
    Select number of students passed exams for each subject and order result by a number
        of student descending
 */
CREATE INDEX idx_student_exams_scores__score_exam_id ON student_exams_scores (score, exam_id);
CREATE INDEX idx_student_exams_scores__exam_id ON student_exams_scores (exam_id);

SELECT subject.title                                                                                  "Exam",
       (SELECT count(*) FROM student_exams_scores sec WHERE sec.score >= 4 AND sec.exam_id = exam.id) "Students passed",
       (SELECT count(*) FROM student_exams_scores sec WHERE sec.exam_id = exam.id)                    "students count"
FROM exams exam
         JOIN subjects subject ON subject.id = exam.subject_id
WHERE (SELECT count(*) FROM student_exams_scores sec WHERE sec.exam_id = exam.id) >= 1
ORDER BY "students count" DESC;
----------

/*
    TASK 4:
    Select the number of students with the same exam marks for each subject.
 */
SELECT subject.title, count(*)
FROM student_exams_scores exam_score
         JOIN exams exam ON exam.id = exam_score.exam_id
         JOIN subjects subject ON subject.id = exam.subject_id
GROUP BY exam_score.score, subject.title;
----------

/*
    TASK 5:
    Select students who passed at least two exams for different subjects
 */
WITH filtered AS (
    SELECT exam_score.student_id
    FROM student_exams_scores exam_score
             JOIN exams exam ON exam.id = exam_score.exam_id
    WHERE exam_score.score >= 4
    GROUP BY exam_score.student_id
    HAVING count(*) >= 2
)
SELECT student.*
FROM students student
WHERE id IN (SELECT filtered.student_id FROM filtered);
----------

/*
 TASK 6:
 Select students who passed at least two exams for the same subject
 */
select student.id,
	   student.first_name || coalesce (' ' || student.second_name, '')|| ' ' || student.last_name "student full name",
	   subject.id,
	   subject.title,
	   count(*) "exams count for subject"
from student_exams_scores ses
join exams e on e.id = ses.exam_id
join subjects subject on subject .id = e.subject_id
join students student on ses.student_id = student.id
group by student.id, student.first_name, student.second_name, student.last_name, subject.id, subject.title
having count(*) > 1
order by "exams count for subject" DESC;
------

/*
 TASK 7:
 Select all subjects which exams passed only students with the same primary skills
 */
SELECT subject.*
FROM subjects subject
         JOIN exams exam ON exam.id = subject.id
         JOIN student_exams_scores exam_score ON exam_score.exam_id = exam.id
WHERE exam_score.score >= 4
  AND subject.id IN (SELECT subject_id
                     FROM student_skills skills
                     WHERE skills.student_id = exam_score.student_id
                       AND skills.skill_type = 'PRIMARY')
GROUP BY subject.id, subject.title;
----------

/*
 TASK 8:
 Select all subjects which exams passed only students with the different primary skills. It means that all students
 passed the exam for the one subject must have different primary skill
 */
SELECT subject.*
FROM subjects subject
         JOIN exams exam ON exam.id = subject.id
         JOIN student_exams_scores exam_score ON exam_score.exam_id = exam.id
WHERE exam_score.score >= 4
  AND subject.id IN (SELECT subject_id
                     FROM student_skills skills
                     WHERE skills.student_id = exam_score.student_id
                       AND skills.skill_type = 'SECONDARY')
GROUP BY subject.id, subject.title;
----------

/*
 TASK 9:
 Select students who do not pass any exam using each of the following operator:
    - Outer join
    - Subquery with ‘not in’ clause
    - Subquery with ‘any ‘ clause Check which approach is faster for 1000, 10K, 100K exams and 10, 1K, 100K students
 */

create view students_passed_at_least_1_exam as
    SELECT exam_score.student_id
    FROM student_exams_scores exam_score
    WHERE exam_score.score >= 4
    group by exam_score.student_id;

-- outer join
select  s.id ,
        s.first_name ,
        s.second_name ,
        s.last_name
from students s
    left outer join students_passed_at_least_1_exam spale on spale.student_id = s.id
where spale.student_id is NULL;

-- NOT IN
SELECT student.*
FROM students student
WHERE student.id NOT IN (SELECT spale.student_id FROM students_passed_at_least_1_exam spale);

-- ANY
SELECT count(student.*)
FROM students student
WHERE student.id = any (SELECT s.id FROM students s where s.id not in (select spale.student_id FROM students_passed_at_least_1_exam spale));
-----------------------

/*
  TASK 10:
  Select all students whose average mark is bigger than the overall average mark
 */
with avg_score(avg) as (
	select sum(ses.score) / count(ses.*)
	from student_exams_scores ses
)
select s.id "student id",
	s.first_name || coalesce (' ' || s.second_name, '')|| ' ' ||s.last_name "student full name",
	sum(ses.score) / count(ses.*) "AVG"
from student_exams_scores ses
join students s on s.id = ses.student_id
group by s.id, s.first_name, s.second_name, s.last_name
having sum(ses.score) / count(ses.*) > (select avg from avg_score)
order by "AVG" DESC;
----------

/*
  TASK 11:
  Select the top 5 students who passed their last exam better than average students
 */
with avg_score(avg) as (
	select sum(ses.score) / count(ses.*)
	from student_exams_scores ses
)
select s.id "student id",
	s.first_name || coalesce (' ' || s.second_name, '')|| ' ' ||s.last_name "student full name",
	sum(ses.score) / count(ses.*) "AVG"
from student_exams_scores ses
join students s on s.id = ses.student_id
group by s.id, s.first_name, s.second_name, s.last_name
having sum(ses.score) / count(ses.*) > (select avg from avg_score)
order by "AVG" desc
limit 5;
----------

/*
  TASK 12:
  Select the biggest mark for each student and add text description for the mark (use COALESCE and WHEN operators)
 */
select s.id,
s.first_name || coalesce (' ' || s.second_name, '')|| ' ' ||s.last_name "student full name",
(select max(ses.score) from student_exams_scores ses where ses.student_id = s.id),
case
when (select max(ses.score) from student_exams_scores ses where ses.student_id = s.id) between 1 and 3 then 'BAD'
when (select max(ses.score) from student_exams_scores ses where ses.student_id = s.id) between 4 and 6 then 'AVERAGE'
when (select max(ses.score) from student_exams_scores ses where ses.student_id = s.id) between 7 and 8 then 'GOOD'
when (select max(ses.score) from student_exams_scores ses where ses.student_id = s.id) between 9 and 10 then 'EXCELLENT'
end as "score description"
from students s;
----------

/*
  TASK 13:
  Select the number of all marks for each mark type (‘BAD’, ‘AVERAGE’,…)
 */
select ses.score,
case
when sum(ses.score) / count(ses.*)  between 1 and 3 then 'BAD'
when sum(ses.score) / count(ses.*)  between 4 and 6 then 'AVERAGE'
when sum(ses.score) / count(ses.*)  between 7 and 8 then 'GOOD'
when sum(ses.score) / count(ses.*)  between 9 and 10 then 'EXCELLENT'
end as "description",
count(*)
from student_exams_scores ses
group by ses.score ;